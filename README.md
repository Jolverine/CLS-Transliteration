# CLS Transliteration

Transliteration tool for Indian languages. Only works between Indian languages and not from or to English. This supports 13 Indian languages, in 8 different scripts. It also supports conversion of text to CLS.

## How to use


```
python charDictMap.py <map-file> <inp-file> <out-file> <inp-lang-tag> <out-lang-tag> <parser-flag>
# see mapping_file for <map-file>.
# input file contains <English><tab><Hindi> for this code. Please change if only Hindi is there.
# <out-file> - Writes the transliterated output this file.
# <inp-lang-tag> & <out-lang-tag>- Give ISO tag for languages. Example: 'hi' for Hindi.
# <parser-flag> - unified-parser or one-to-one based on Indo-Aryan or Dravidian languages.
```

